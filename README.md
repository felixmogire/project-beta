# CarCar

Team:




* Felix Mogire - Service
* Brian Miller - Sales


## Design

## Service microservice

This portion of the app is designed to create and manage service repair appointments for customer vehicles.
This microservice takes in automobile data from the inventory microservice and creates AutoMobileVO that are used to determine VIP status when scheduling appointments.
The various models are listed below:

Technician Model
This model contains data for the technician that is assigned to an appointment.


Appointments Model
This model contains data pertaining to an appointment and uses technician as a foreign key to the Technician model.

AutomobileVO Model
This model contains data pertaining to a value object that is created from data being polled from the Inventory microservice every 20 seconds. The 20 second poll rate means there is a 20 second window in which an automobile is being added to the inventory and an appointment being made where the vip status will be incorrectly assigned. It should be a short enough poll that it isn't an issue, but the poll rate can be adjusted as needed in /service/api/poll/poller.py/.
This model contains two fields "vin" and "sold". The reason we are automatically generating objects from the Inventory is because we want to be able to assign an appointment a VIP value.
Under the assumption that a customer that is making an appointment has already bought the vehicle means if the "vin" in one of our appointments matches a "vin" in one of our AutomobileVO objects we can assume the car has been sold and to treat that as a VIP appointment.

## Sales microservice

The sales portion of the app is designed to create and track sales of the automobiles. The Customer, Salesperson, and automobileVO models track their corresponding details while the Sale model connects them together to track a specific automobile sale.
