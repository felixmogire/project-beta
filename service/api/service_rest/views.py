from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
from django.db import IntegrityError
import json
from .models import Technician, Appointment, AutomobileVO
from .encoders import TechnicianEncoder, AppointmentEncoder


@require_http_methods(["GET", "POST", "DELETE"])
def list_technicians(request, id=None):
    if request.method == "GET":
        try:
            technicians = Technician.objects.all()
            return JsonResponse({"technicians": list(technicians)}, encoder=TechnicianEncoder, safe=False)
        except:
            return JsonResponse({"message": "Error getting technician info"}, status=400)

    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse({"deleted": True})
        except Technician.DoesNotExist:
            return JsonResponse({"deleted": False}, status=400)

    else:
        try:
            body = json.loads(request.body)
            technician = Technician.objects.create(**body)
            return JsonResponse(technician, encoder=TechnicianEncoder, safe=False)
        except IntegrityError as e:
            if 'unique constraint' in e.args[0]:
                return JsonResponse({"message": "Employee ID already used. Choose a different ID."}, status=400)
        except:
            return JsonResponse({"message": 'Error creating employee'}, status=400)

@require_http_methods(['GET', 'POST'])
def list_appointments(request):
    if request.method == 'GET':
        try:
            appointments = Appointment.objects.all().order_by("vin")
            return JsonResponse({'appointments': list(appointments)}, encoder=AppointmentEncoder, safe=False)
        except:
            return JsonResponse({'message': 'Error getting appointments'}, status=400)

    else:
        body = json.loads(request.body)
        try:
            technician = Technician.objects.get(id=body['technician'])
        except Technician.DoesNotExist:
            return JsonResponse({'message': 'Technician does not exist'}, status=400)

        body['technician'] = technician

        try:
            vip_check = AutomobileVO.objects.get(vin=body['vin'])
            body['vip'] = True
        except AutomobileVO.DoesNotExist:
            body['vip'] = False

        appointment = Appointment.objects.create(**body)

        return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)

@require_http_methods(['DELETE', 'PUT'])
def edit_appointment(request, id, status=None):
    if request.method == 'PUT' and status == 'finish':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = 'finished'
            appointment.save()
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({'message': 'Invalid appointment ID'}, status=400)

    elif request.method == 'PUT' and status == 'cancel':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = 'canceled'
            appointment.save()
            return JsonResponse(appointment, encoder=AppointmentEncoder, safe=False)
        except Appointment.DoesNotExist:
            return JsonResponse({'message': 'Invalid appointment ID'}, status=400)

    else:
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse({'deleted': True})
        except Appointment.DoesNotExist:
            return JsonResponse({'deleted': False}, status=400)
