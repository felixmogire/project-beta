from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from .encoders import (
    AutomobileVOEncoder,
    SalespersonEncoder,
    CustomerEncoder,
    SaleEncoder,
)

from .models import AutomobileVO, Salesperson, Customer, Sale

@require_http_methods(["GET", "POST"])
def api_salesperson(request):
    if request.method == "GET":
        salespeople = Salesperson.objects.all()
        return JsonResponse(
            {"salespeople": list(salespeople.values())},
            encoder=SalespersonEncoder,
        )
    else:
        content = json.loads(request.body)
        salesperson = Salesperson.objects.create(**content)
        return JsonResponse(
            salesperson,
            encoder=SalespersonEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_salesperson_delete(request, pk):
    try:
        salesperson = Salesperson.objects.get(pk=pk)
        salesperson.delete()
        return JsonResponse({"deleted": True})
    except Salesperson.DoesNotExist:
        return JsonResponse({"deleted": False}, status=404)


@require_http_methods(["GET", "POST"])
def api_customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            {"customers": list(customers.values())},
            encoder=CustomerEncoder,
        )
    else:
        content = json.loads(request.body)
        customer = Customer.objects.create(**content)
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False,
        )

@require_http_methods(["DELETE"])
def api_customer_delete(request, pk):
    try:
        customer = Customer.objects.get(pk=pk)
        customer.delete()
        return JsonResponse({"deleted": True})
    except Customer.DoesNotExist:
        return JsonResponse({"deleted": False}, status=404)


@require_http_methods(["GET", "POST"])
def api_sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            {"sales": list(sales.values())},
            encoder=SaleEncoder,
        )
    else:
        content = json.loads(request.body)
        try:
            automobile_vin = content['automobile']
            automobile = AutomobileVO.objects.get(vin=automobile_vin)
            content['automobile'] = automobile
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "No automobile exists"},
                status=400,
            )

        try:
            salesperson = Salesperson.objects.get(id=content['salesperson'])
            content['salesperson'] = salesperson
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "No salesperson exists"},
                status=400,
            )

        try:
            customer = Customer.objects.get(id=content['customer'])
            content['customer'] = customer
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "No customer exists"},
                status=400,
            )

        sale = Sale.objects.create(**content)
        return JsonResponse(
            {"sales": list(Sale.objects.filter(pk=sale.pk).values())},
            encoder=SaleEncoder,
        )

@require_http_methods(["DELETE"])
def api_sales_delete(request, pk):
    try:
        sale = Sale.objects.get(pk=pk)
        sale.delete()
        return JsonResponse({"deleted": True})
    except Sale.DoesNotExist:
        return JsonResponse({"deleted": False}, status=404)
